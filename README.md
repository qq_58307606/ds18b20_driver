# ds18b20_driver

[![License](https://img.shields.io/badge/License-MIT-blue.svg)](LICENSE)
[![Platform](https://img.shields.io/badge/Platform-STM32/ESP32/Arduino-4B325D.svg)]()

## 📂 项目目录
```
ds18b20
├── ds18b20.c
├── ds18b20.h
├── LICENSE
└── README.md
```
## 🚀 项目简介
本项目是一个针对ds18b20开发的驱动库，支持多平台调用，只需要一个GPIO引脚便可以实现温度的读取，且使用状态机流程控制，区别于传统的堵塞读取方式，释放了CPU资源。

## ✨ 主要特性
- [x] 支持[STM32/ESP32/国产MCU]
- [x] 支持[裸机平台/RTOS系统]
- [x] 支持[状态机流程]

## 📦 快速开始

### 硬件要求
- [任何MCU]
- [外设要求:GPIO引脚]

### 安装方法
将ds18b20.c和ds18b20.h文件添加到工程
   ```
#include "ds18b20.h"

/********************************************************************************************************
*  @函数名   ds18b20_callback						                                                           
*  @描述     获取DS18B20温度                     
*  @参数     无
*  @返回值   无   
*  @注意     无
********************************************************************************************************/
void ds18b20_callback(int32_t data)
{
	/* 获取数据 */
    collect.air_data.water_temp = (int16_t)data;
    air_event.water_flag = 1;
}

void main() 
{
    fml_ds18b20_callback_register(ds18b20_callback); // 注册回调函数
    
    fml_ds18b20_start();// 开始温度采集
    
    while(1) {
       /* 1ms任务 */
		if (systick_flag)
		{
        	systick_flag = 0;
            
            fml_ds18b20_proc();	// 温度采集
         }
    }
}
   ```
## 🛠 配置说明
在ds18b20.h中可修改以下配置：

   ```
// 硬件接口配置
#define	DS18B20_DQ_PORT					GPIO_PORT_A
#define	DS18B20_DQ_PIN					GPIO_PIN_09
#define	DS18B20_DQ_GET()				(((*((__IO uint16_t *)((uint32_t)(&CM_GPIO->PIDRA) + 0x10UL * DS18B20_DQ_PORT)) & (DS18B20_DQ_PIN)) != 0U) ? PIN_SET : PIN_RESET)
#define	DS18B20_DQ_SET()				(*((__IO uint16_t *)((uint32_t)(&CM_GPIO->POSRA) + 0x10UL * DS18B20_DQ_PORT)) |= ((uint16_t)(DS18B20_DQ_PIN)))
#define	DS18B20_DQ_CLR()				(*((__IO uint16_t *)((uint32_t)(&CM_GPIO->PORRA) + 0x10UL * DS18B20_DQ_PORT)) |= ((uint16_t)(DS18B20_DQ_PIN)))
#define	DS18B20_IN()					((*((__IO uint16_t *)((uint32_t)(&CM_GPIO->POERA) + 0x10UL * DS18B20_DQ_PORT))) &= ((uint16_t)(~((uint16_t)(DS18B20_DQ_PIN)))))
#define	DS18B20_OUT()					(*((__IO uint16_t *)((uint32_t)(&CM_GPIO->POERA) + 0x10UL * DS18B20_DQ_PORT)) |= ((uint16_t)(DS18B20_DQ_PIN)))
   ```
## 🤝 贡献指南
欢迎通过Issue或PR参与贡献，请遵循：

Fork项目并创建分支

提交代码前运行测试用例

使用标准C99编码规范

更新相关文档

## 📄 许可证
本项目采用 MIT 许可证

## 📧 联系作者
作者: x_master

邮箱: 1030577566@qq.com

个人主页:暂无


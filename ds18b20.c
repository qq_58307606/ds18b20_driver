#include "ds18b20.h"

ds18b20_callback_t ds18b20_cbs;
stc_ds18b20_t ds18b20;

/********************************************************************************************************
*  @函数名   ds18b20_check						                                                           
*  @描述     ds18b20初始化
*  @参数     无
*  @返回值   1-成功 0-失败
*  @注意     无
********************************************************************************************************/
static uint8_t ds18b20_check(void)
{
    uint8_t time_out = 0;
	
    DS18B20_OUT();
    DS18B20_DQ_SET();
    hdl_delay_us(10);
    
    DS18B20_DQ_CLR();
    hdl_delay_us(600);
    
    DS18B20_IN();
    
    while(1)
    {
        if(DS18B20_DQ_GET() == 0)
        {
            time_out = 0;
            return 1;
        }else{
          hdl_delay_us(1);
          time_out++;
          if( time_out > 200 )  
              return 0;
        }
    }
}

/********************************************************************************************************
*  @函数名   ds18b20_read_bit						                                                           
*  @描述     ds18b20读一个bit
*  @参数     无
*  @返回值   bit位
*  @注意     无
********************************************************************************************************/
static uint8_t ds18b20_read_bit(void)
{
    uint8_t data;
    DS18B20_OUT();   //设置为输出
    DS18B20_DQ_CLR();// DS18B20_DQ_OUT=0;
    hdl_delay_us(5);
    DS18B20_DQ_SET();//DS18B20_DQ_OUT=1;
    DS18B20_IN();    //设置为输入
    hdl_delay_us(5);
    if(DS18B20_DQ_GET())
		data = 1;
    else 
		data = 0;
    hdl_delay_us(50);
    return data;
}

/********************************************************************************************************
*  @函数名   ds18b20_read_byte						                                                           
*  @描述     ds18b20读一个byte
*  @参数     无
*  @返回值   一个字节数据
*  @注意     无
********************************************************************************************************/
static uint8_t ds18b20_read_byte(void)
{
    uint8_t i, j, dat;
    dat = 0;
    for(i = 1; i <= 8; i++)
    {
        j = ds18b20_read_bit();
        dat = (j << 7) | (dat >> 1);
    }
    return dat;
}

/********************************************************************************************************
*  @函数名   ds18b20_write_byte						                                                           
*  @描述     ds18b20写一个byte
*  @参数     dat-要写入的数据
*  @返回值   无
*  @注意     无
********************************************************************************************************/
static void ds18b20_write_byte(uint8_t dat)
{
    uint8_t j;
    uint8_t testb;
    DS18B20_OUT();     //设置为输出
    for(j = 1; j <= 8; j++)
    {
        testb = dat & 0x01;
        dat = dat >> 1;
        if(testb)       // 写1
        {
            DS18B20_DQ_CLR();//DS18B20_DQ_OUT=0;
            hdl_delay_us(5);
            DS18B20_DQ_SET();//	DS18B20_DQ_OUT=1;
            hdl_delay_us(60);
        }
        else            //写0
        {
            DS18B20_DQ_CLR();//DS18B20_DQ_OUT=0;
            hdl_delay_us(60);
            DS18B20_DQ_SET();//DS18B20_DQ_OUT=1;
            hdl_delay_us(5);
        }
    }
}

/********************************************************************************************************
*  @函数名   fml_ds18b20_start						                                                           
*  @描述     启动转换
*  @参数     无
*  @返回值   无
*  @注意     无
********************************************************************************************************/
void fml_ds18b20_start(void)
{
	ds18b20.step = DS18B20_INIT;
}

/********************************************************************************************************
*  @函数名   fml_ds18b20_proc						                                                           
*  @描述     温度轮询任务
*  @参数     无
*  @返回值   无
*  @注意     无
********************************************************************************************************/
uint8_t fml_ds18b20_proc(void)
{
	static uint16_t ds18b20_time_out = 0;
	int16_t temp_value = 0;	// 温度值缓存
	
	switch(ds18b20.step)
	{
		case DS18B20_IDLE:	// 空闲
			ds18b20_time_out = 0;
			break;
		case DS18B20_INIT:	// 初始化
            if (ds18b20_check() == 0)
            {
                return 0;
            }
            ds18b20.step = DS18B20_WAIT;
            break;
            
        case DS18B20_WAIT:
            hdl_delay_us(1);
            ds18b20.step = DS18B20_SKIDROM;
            break;
           
       case DS18B20_SKIDROM:	// 开始采样
            ds18b20_write_byte(0xCC);
            ds18b20_write_byte(0x44);  // begin to convert temperature data
            ds18b20_time_out = 0;
            ds18b20.step = DS18B20_CONVERT;
            break;
       
       case DS18B20_CONVERT:	// 等待转换
           ds18b20_time_out++;
           if(ds18b20_time_out > WAIT_CNT_CONVERT)
           {
               ds18b20_time_out = 0;
               ds18b20.step = DS18B20_RESET;
           }
           break;
        
        case DS18B20_RESET:		// 检查总线
            if (ds18b20_check() == 0)
            {
                return 0;
            }
            ds18b20.step = DS18B20_WAIT_DATA;
            break;
            
        case DS18B20_WAIT_DATA:	
            hdl_delay_us(1);
            ds18b20.step = DS18B20_CMD;
            break;
            
        case DS18B20_CMD:		// 发送读取数据命令
            ds18b20_write_byte(0xCC);
            ds18b20_write_byte(0xBE);  // read temperature data register
            ds18b20.step = DS18B20_READ;
            break;
            
        case DS18B20_READ:		// 读取数据
            ds18b20.data_l = ds18b20_read_byte();
            ds18b20.data_h = ds18b20_read_byte();
 
            temp_value = ((ds18b20.data_h << 8) | ds18b20.data_l);
			ds18b20.pn_sta = (temp_value & 0x8000) > 0 ? 1: 0;
		
			if(ds18b20.pn_sta)
			{
			   temp_value = ~temp_value;   // 取补码
			   temp_value += 1;           // 得到原码
			}
			ds18b20.data = (int32_t)temp_value * 0.625;
			
			if(ds18b20_cbs)	// 转换完成
			{
				ds18b20_cbs();
			}
			
            ds18b20.step = DS18B20_IDLE;
		default:
			break;
	}
	
	return 1;
}

/********************************************************************************************************
*  @函数名   fml_ds18b20_data_get						                                                           
*  @描述     获取温度值
*  @参数     无
*  @返回值   无
*  @注意     无
********************************************************************************************************/
int16_t fml_ds18b20_data_get(void)
{
	int16_t data = 0;
	
	data = ds18b20.data;
	
	return data;
}

/********************************************************************************************************
*  @函数名   fml_ds18b20_callback_register						                                                           
*  @描述     注册转换完成标志位
*  @参数     无
*  @返回值   无
*  @注意     无
********************************************************************************************************/
void fml_ds18b20_callback_register(ds18b20_callback_t pCBS)
{
	if(ds18b20_cbs == 0)
	{
		ds18b20_cbs = pCBS;
	}
}


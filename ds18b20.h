#ifndef ____HDL_DS18B20_H_
#define ____HDL_DS18B20_H_

#include "hc32_ll.h"

#include "hdl_clk.h"
#include "filter.h"

#define	DS18B20_DQ_PORT					GPIO_PORT_A
#define	DS18B20_DQ_PIN					GPIO_PIN_09
#define	DS18B20_DQ_GET()				(((*((__IO uint16_t *)((uint32_t)(&CM_GPIO->PIDRA) + 0x10UL * DS18B20_DQ_PORT)) & (DS18B20_DQ_PIN)) != 0U) ? PIN_SET : PIN_RESET)
#define	DS18B20_DQ_SET()				(*((__IO uint16_t *)((uint32_t)(&CM_GPIO->POSRA) + 0x10UL * DS18B20_DQ_PORT)) |= ((uint16_t)(DS18B20_DQ_PIN)))
#define	DS18B20_DQ_CLR()				(*((__IO uint16_t *)((uint32_t)(&CM_GPIO->PORRA) + 0x10UL * DS18B20_DQ_PORT)) |= ((uint16_t)(DS18B20_DQ_PIN)))
#define	DS18B20_IN()					((*((__IO uint16_t *)((uint32_t)(&CM_GPIO->POERA) + 0x10UL * DS18B20_DQ_PORT))) &= ((uint16_t)(~((uint16_t)(DS18B20_DQ_PIN)))))
#define	DS18B20_OUT()					(*((__IO uint16_t *)((uint32_t)(&CM_GPIO->POERA) + 0x10UL * DS18B20_DQ_PORT)) |= ((uint16_t)(DS18B20_DQ_PIN)))

#define WAIT_CNT_CONVERT 700	// 等待转换时间

typedef enum
{
	DS18B20_IDLE,	// 空闲
	DS18B20_INIT,	// 初始化
	DS18B20_WAIT,	// 等待释放总线
	DS18B20_SKIDROM,// 发送转换命令
	DS18B20_CONVERT,// 触发温度转换
	DS18B20_RESET,	// 复位
	DS18B20_WAIT_DATA,// 等待数据
	DS18B20_CMD,	// 发送读数据命令
	DS18B20_READ,	// 读取数据
}en_ds18b20_step_t;

typedef struct
{
	en_ds18b20_step_t step;	// 工作步骤
	uint8_t data_l;
	uint8_t data_h;
	uint8_t pn_sta;			// 符号位
	int32_t data;
}stc_ds18b20_t;

typedef void (*ds18b20_callback_t)(void);

void fml_ds18b20_callback_register(ds18b20_callback_t pCBS);

void fml_ds18b20_start(void);
uint8_t fml_ds18b20_proc(void);
int16_t fml_ds18b20_data_get(void);

#endif
